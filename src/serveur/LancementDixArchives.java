package serveur;


import modele.*;
import org.glassfish.grizzly.http.server.HttpServer;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import configuration.Initialisation;

import configuration.Orchestrateur;
import configuration.ServiceBibliotheque;
import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.LienVersRessource;

public class LancementDixArchives {

	public static void main(String[] args) {
		
		ResourceConfig config = new ServiceBibliotheque();
		
		int v = 0;
		HyperLien<?> h = Initialisation.serveurs().get(0);
		//for(HyperLien<?> h : Initialisation.serveurs()){
			HttpServer serveur1 = GrizzlyHttpServerFactory.createHttpServer(h.getUri(), config);	
			System.out.println("* Serveur Grizzly démarré : " + serveur1.isStarted());			
			System.out.println("** Adresse : " + h.getUri());
			ajouterLivres(Initialisation.biblio(h), v);
			v++;
		//}

	}

	private static void ajouterLivres(HyperLien<BibliothequeArchive> h, int v) {
		Archive b = 
				LienVersRessource.proxy(Orchestrateur.clientJAXRS(), h, BibliothequeArchive.class);
		for(int i = 0; i < 10; i++){
			System.out.println(b.toString());

			//b.getRepresentation(new ImplemIdentifiantLivre("200"));
			Livre livre = new ImplemLivre("Services" + v + "." + i);
			try{
				//b.getRepresentation(new ImplemIdentifiantLivre("200"));
				HyperLien<Livre> a = b.ajouter(livre);
			} catch (Exception e) {

				e.printStackTrace();
			}


		}
	}
	
}
