package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;

public class RechercheSynchroneStreamParallele extends RechercheSynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	
	public RechercheSynchroneStreamParallele(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		    return bibliotheques.parallelStream().map(h ->rechercheSync(h, l, client)).filter(f -> !f.isEmpty()).findAny().get();
		  
		  }


	@Override
	public NomAlgorithme nom() {
		
		return new ImplemNomAlgorithme("recherche sync stream 8");
	}
	

}
