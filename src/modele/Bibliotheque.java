package modele;

import java.util.Optional;
import java.util.concurrent.Future;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.HyperLiens;
import infrastructure.jaxrs.annotations.ReponsesPOSTEnCreated;
import infrastructure.jaxrs.annotations.ReponsesPUTOption;

import static configuration.JAXRS.*;

public interface Bibliotheque {
	@PUT
	@Consumes(TYPE_MEDIA)
	@Produces(TYPE_MEDIA)
	@ReponsesPUTOption
	Optional<HyperLien<Livre>> chercher(Livre l);
	@PUT
	@Consumes(TYPE_MEDIA)
	@Produces(TYPE_MEDIA)
	@ReponsesPUTOption
	@Path(SOUSCHEMIN_ASYNC)
	Future<Optional<HyperLien<Livre>>> chercherAsynchrone(Livre l, @Suspended final AsyncResponse ar);
	@GET
	@Produces(TYPE_MEDIA)
	@Path(SOUSCHEMIN_CATALOGUE)
	HyperLiens<Livre> repertorier();
	
}
