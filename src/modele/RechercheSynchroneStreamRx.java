package modele;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RechercheSynchroneStreamRx extends RechercheSynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	public RechercheSynchroneStreamRx(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		
		
		Observable<Optional<HyperLien<Livre>>> obs =  Observable.fromIterable(bibliotheques)
		         .flatMap(h -> Observable.fromCallable(() -> rechercheSync(h, l, client)).subscribeOn(Schedulers.io())).filter(h -> !h.isPresent());
				return obs.blockingFirst();
		
		
	}
	@Override
	public NomAlgorithme nom() {
		
		return new ImplemNomAlgorithme("recherche sync stream rx");
	}
	

}
