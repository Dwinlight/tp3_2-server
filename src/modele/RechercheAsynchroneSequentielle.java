package modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;

public class RechercheAsynchroneSequentielle extends RechercheAsynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	public RechercheAsynchroneSequentielle(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		List<Future<Optional<HyperLien<Livre>>>> list = new ArrayList();
		bibliotheques.forEach(e -> list.add(rechercheAsync(e, l, client)));
		List<Optional<HyperLien<Livre>>> rez = new ArrayList<Optional<HyperLien<Livre>>>();
		list.forEach(p -> {
			try {
				rez.add(p.get());
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		Optional<HyperLien<Livre>>retourne = Optional.empty();
		Iterator<Optional<HyperLien<Livre>>> it = rez.iterator();
		while(it.hasNext() && retourne.isEmpty()) {
			retourne = it.next();
		}
		return retourne;
	}
	@Override
	public NomAlgorithme nom() {
		return new ImplemNomAlgorithme("recherche async seq");
	}
	
}
