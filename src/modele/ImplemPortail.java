package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import javax.inject.Singleton;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.container.AsyncResponse;

import configuration.Orchestrateur;
import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.HyperLiens;
import infrastructure.jaxrs.LienVersRessource;

@Singleton
@Path("portail")
public class ImplemPortail implements Portail{
	 ConcurrentMap<NomAlgorithme, AlgorithmeRecherche> algos;
	 Client client;
	 AlgorithmeRecherche algo;
	 List<HyperLien<BibliothequeArchive>> links;
	 
	public ImplemPortail() {
		this.algos = new ConcurrentHashMap<NomAlgorithme, AlgorithmeRecherche>();
		this.client = Orchestrateur.clientJAXRS();
		this.algo = null;
		this.links = configuration.Initialisation.bibliotheques();
	}

	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l) {
		return this.algo.chercher(l, this.links, this.client);
	}

	@Override
	public Future<Optional<HyperLien<Livre>>> chercherAsynchrone(Livre l, AsyncResponse ar) {
	
		return ImplementationAppelsAsynchrones.rechercheAsynchroneBibliotheque(this,l,ar);
		
	}

	@Override
	public HyperLiens<Livre> repertorier() {
		
		List<HyperLien<Livre>> con = new ArrayList();
		this.links.parallelStream().map(h->LienVersRessource.proxy(this.client,h, BibliothequeArchive.class).repertorier().getLiens().stream().map(l -> con.add(l)));
		return new HyperLiens<Livre>(con);
	}

	@Override
	public void changerAlgorithmeRecherche(NomAlgorithme algo) {
		if(this.algos.containsKey(algo)) {
			this.algo = this.algos.get(algo);
		}
		
	}

}
