package modele;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;

public class RechercheSynchroneSequentielle extends RechercheSynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	public RechercheSynchroneSequentielle(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		int i = 0;
		Optional<HyperLien<Livre>> rez = Optional.empty();
		while (i<bibliotheques.size() && rez.isEmpty()) {
			rez = super.rechercheSync(bibliotheques.get(i), l, client);
		}
		return rez;
	}
	@Override
	public NomAlgorithme nom() {
		
		return new ImplemNomAlgorithme("recherche sync seq");
	}
	

}
