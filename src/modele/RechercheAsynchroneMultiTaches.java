package modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.Outils;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RechercheAsynchroneMultiTaches extends RechercheAsynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	
	public RechercheAsynchroneMultiTaches(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		List<Optional<HyperLien<Livre>>> rez = new ArrayList<Optional<HyperLien<Livre>>>();
		Observable.fromIterable(bibliotheques)
		.flatMap(
				h -> Observable.fromFuture(rechercheAsync(h, l, client)).subscribeOn(Schedulers.io())
		)
		.filter(a -> !a.isEmpty()).map(el -> rez.add(el));
		return rez.isEmpty() ? Optional.empty() : rez.get(0);
	}
	@Override
	public NomAlgorithme nom() {
		return new ImplemNomAlgorithme("recherche async stream rx");
	}
	
}
