package modele;

import java.util.Optional;
import java.util.concurrent.Future;

import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.LienVersRessource;
import infrastructure.langage.Types;

import javax.ws.rs.client.*;

import static configuration.JAXRS.SOUSCHEMIN_ASYNC;
import static configuration.JAXRS.TYPE_MEDIA;

public abstract class RechercheAsynchroneAbstraite implements AlgorithmeRecherche {

	 protected Future<Optional<HyperLien<Livre>>> rechercheAsync(HyperLien<BibliothequeArchive> h, Livre l, Client client) {

	 	 WebTarget target = client.target(h.getUri()).path(SOUSCHEMIN_ASYNC);
		 Future<Optional<HyperLien<Livre>>> response = target.request(TYPE_MEDIA)
				 .accept(TYPE_MEDIA)
				 .async()
				 .put(Entity.entity(l, TYPE_MEDIA), Types.typeRetourChercherAsync());

		 return response;
	};

	 //LOOK DOC HOW TO CRATE RETOUR
	 protected Future<Optional<HyperLien<Livre>>> rechercheAsyncAvecRappel(
			HyperLien<BibliothequeArchive> h, Livre l, Client client,  
			InvocationCallback<Optional<HyperLien<Livre>>> retour) {

	 	WebTarget target = client.target(h.getUri()).path(SOUSCHEMIN_ASYNC);
	 	AsyncInvoker asyncInvoker = target.request().accept(TYPE_MEDIA).async();
		return asyncInvoker.put(Entity.entity(l, TYPE_MEDIA), retour);
	 };
}