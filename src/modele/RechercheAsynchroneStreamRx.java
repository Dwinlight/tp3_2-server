package modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;

import infrastructure.jaxrs.HyperLien;

public class RechercheAsynchroneStreamRx extends RechercheAsynchroneAbstraite implements AlgorithmeRecherche{
	private NomAlgorithme nom;
	private ExecutorService service = Executors.newCachedThreadPool();
	
	public RechercheAsynchroneStreamRx(String s) {
		this.nom = new ImplemNomAlgorithme(s);
	}
	@Override
	public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<BibliothequeArchive>> bibliotheques,
			Client client) {
		CountDownLatch countDownLatch = new CountDownLatch(bibliotheques.size());
		Optional<HyperLien<Livre>> reponse = Optional.empty();
		for(HyperLien<BibliothequeArchive> h: bibliotheques) {
			try {
				reponse = this.service.submit(()-> {
					Future<Optional<HyperLien<Livre>>> var = rechercheAsync(h, l, client);
					if (var.get().isEmpty()) {
						countDownLatch.countDown();
					}
					else {
						int count = (int) countDownLatch.getCount();
						for(long j=0; j<count; j++) {
							countDownLatch.countDown();
						}
						
					}
					return var.get();
					
				}).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reponse;
		    
	}
	@Override
	public NomAlgorithme nom() {
		return new ImplemNomAlgorithme("recherche async multi");
	}
	
}
