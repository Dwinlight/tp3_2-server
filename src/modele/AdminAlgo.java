package modele;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import static configuration.JAXRS.SOUSCHEMIN_ALGO_RECHERCHE;
import static configuration.JAXRS.TYPE_MEDIA;

public interface AdminAlgo {
	@PUT
	@Path(SOUSCHEMIN_ALGO_RECHERCHE)
	@Consumes(TYPE_MEDIA)
	@Produces(TYPE_MEDIA)
	void changerAlgorithmeRecherche(NomAlgorithme algo);
}
