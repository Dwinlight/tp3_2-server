package infrastructure.jaxrs;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;

import org.glassfish.hk2.utilities.reflection.ParameterizedTypeImpl;

import infrastructure.langage.Types;
import modele.Livre;

@Priority(Priorities.HEADER_DECORATOR + 2)
public class AdapterClientReponsesPUTEnOption implements ReaderInterceptor {


	@Override
	public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
		if (context.getType().equals(Optional.class)) {
			ParameterizedType typeParam = (ParameterizedType) context.getGenericType();
			Type type = typeParam.getActualTypeArguments()[0];
			typeParam = new ParameterizedTypeImpl(type);
			context.setType(Types.convertirTypeEnClasse(type));
			return Optional.of(context.proceed());
		} else {
			System.out.println("aroundReadFrom AdapterClientReponsesPUTEnOption");
			System.out.println("context est null : ");
			System.out.println(context.proceed().equals(null));
			return Optional.of(new HyperLien<Livre>());
		}
	}

}
